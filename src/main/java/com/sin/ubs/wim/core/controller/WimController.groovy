package com.sin.ubs.wim.core.controller

import com.sin.ubs.wim.core.model.SummaryReportModel
import com.sin.ubs.wim.core.model.entity.ConfigEntity
import com.sin.ubs.wim.core.model.entity.WimEntity
import com.sin.ubs.wim.core.model.entity.WimEntity_
import com.sin.ubs.wim.core.repository.WimRepository
import com.sin.ubs.wim.core.service.CameraService
import com.sin.ubs.wim.core.service.WimService
import groovy.util.logging.Slf4j
import org.apache.batik.transcoder.TranscoderInput
import org.apache.batik.transcoder.TranscoderOutput
import org.apache.batik.transcoder.image.JPEGTranscoder
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView

import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by sin on 1/25/14.
 */
@RestController
@RequestMapping('/wim')
@Slf4j
class WimController {

    @Autowired
    private WimRepository wimRepository

    @Autowired
    private WimService wimService

    @Autowired
    private CameraService cameraService

    @RequestMapping('/listen')
    void listen(HttpServletRequest request) {
        request.startAsync().with {
            setTimeout(0)
            getResponse().setContentType('text/event-stream')
            wimService.wimListeners << ['process': {
                log.trace("send request:${it}")
                getResponse().getWriter().println(it)
                getResponse().flushBuffer()
            }, 'close'                           : {
                complete()
            }]
        }
    }

    @RequestMapping('/report/{format}/{from}/{to}/wim-summary')
    def getSummaryReport(
            @PathVariable String format,
            @PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date from,
            @PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date to,
            @RequestParam(required = false) boolean overloadOnly) {
        from = from.clearTime()
        to = to.clearTime()
        List<WimEntity> wimdata
        if (overloadOnly)
            wimdata = wimRepository.findByMeasurementtimeOverload(from, to);
        else
            wimdata = wimRepository.findByMeasurementtime(from, to);
        def tmpMap = [:]
        wimdata.each {
            def date = it.measurementTime.clearTime()
            tmpMap[date] = tmpMap[date] ?: new SummaryReportModel(date: date)
            tmpMap[date] += it
        }
        (from..to).each {
            tmpMap[it] = tmpMap[it] ?: new SummaryReportModel(date: it)
        }
        def param = [data: tmpMap.values().sort(), description: '', format: format]
        if (format == 'xls') param['IS_IGNORE_PAGINATION'] = true
        new ModelAndView('wim-summary', param)
    }

    @RequestMapping('/report/{format}/{from}/{to}/wim-detail')
    def getDetailReportDaily(
            @PathVariable String format,
            @PathVariable String from,
            @PathVariable String to,
            HttpServletResponse response,
            @RequestParam(required = false) boolean overloadOnly) {
        def param = ['whereClause': "where date(measurementtime) between '$from' and '$to' " + (overloadOnly ? 'and vehicleOverload=true' : '') + " order by measurementtime", 'description': "Data between $from to $to", 'format': format]
        if (format == 'xls') param['IS_IGNORE_PAGINATION'] = true
        new ModelAndView('wim-detail', param)
    }

    @RequestMapping('/report/month/{format}/{month}/{year}/wim-detail')
    def getDetailReportMonthly(@PathVariable String month,
                               @PathVariable String year,
                               @PathVariable String format,
                               @RequestParam(required = false) boolean overloadOnly) {
        new ModelAndView('wim-detail', ['whereClause': "where date_part('month',measurementtime) = $month " +
                "and date_part('year',measurementtime)= $year " + (overloadOnly ? 'and vehicleOverload=true' : '') + " order by measurementtime",
                                        'description': "Data monthly: $year / $month", 'format': format])
    }

    @RequestMapping('/statistic/daily/{day}')
    def dailyStatistic(@PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date day) {
        def wimdata = wimRepository.findByMeasurementtime(day, day);
        def result = [normal: [], overload: []]
        24.times { hour ->
            result.normal << wimdata.findAll {
                !it.vehicleOverload && new DateTime(it.measurementTime).hourOfDay == hour
            }.size()
            result.overload << wimdata.findAll {
                it.vehicleOverload && new DateTime(it.measurementTime).hourOfDay == hour
            }.size()
        }
        result
    }

    @RequestMapping('/statistic/daily/golongan/{day}')
    def dailyStatisticPerGolongan(@PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date day) {
        def wimdata = wimRepository.findByMeasurementtime(day, day);
        def result = [:]
        def golongan = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII']
        golongan.each {
            result[it] = [normal: [], overload: []]
        }
        24.times { hour ->
            golongan.each { gol ->
                result[gol].normal << wimdata.findAll {
                    !it.vehicleOverload && new DateTime(it.measurementTime).hourOfDay == hour && it.golongan == gol
                }.size()
                result[gol].overload << wimdata.findAll {
                    it.vehicleOverload && new DateTime(it.measurementTime).hourOfDay == hour && it.golongan == gol
                }.size()
            }
        }
        result
    }

    @RequestMapping('/statistic/weekly/{from}/{to}')
    def weeklyStatistic(
            @PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date from,
            @PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date to) {
        def wimdata = wimRepository.findByMeasurementtime(from, to);
        def result = [normal: [], overload: []]
        while (from <= to) {
            result.normal << wimdata.findAll {
                !it.vehicleOverload && new DateTime(it.measurementTime).dayOfMonth() == new DateTime(from).dayOfMonth()
            }.size()
            result.overload << wimdata.findAll {
                it.vehicleOverload && new DateTime(it.measurementTime).dayOfMonth() == new DateTime(from).dayOfMonth()
            }.size()
            from++
        }
        result
    }

    @RequestMapping('/statistic/weekly/golongan/{from}/{to}')
    def weeklyStatisticPerGolongan(
            @PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date from,
            @PathVariable @DateTimeFormat(pattern = 'yyyy-MM-dd') Date to) {
        def wimdata = wimRepository.findByMeasurementtime(from, to);
        def result = [:]
        def golongan = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII']
        golongan.each {
            result[it] = [normal: [], overload: []]
        }
        while (from <= to) {
            golongan.each { gol ->
                result[gol].normal << wimdata.findAll {
                    !it.vehicleOverload && new DateTime(it.measurementTime).dayOfMonth() == new DateTime(from).dayOfMonth() && it.golongan == gol
                }.size()
                result[gol].overload << wimdata.findAll {
                    it.vehicleOverload && new DateTime(it.measurementTime).dayOfMonth() == new DateTime(from).dayOfMonth() && it.golongan == gol
                }.size()
            }
            from++
        }
        result
    }

    @RequestMapping('/statistic/monthly/{month}/{year}')
    def monthlyStatistic(@PathVariable int month, @PathVariable int year) {
        def from = new DateTime().withMonthOfYear(month).withYear(year).withDayOfMonth(1)
        def to = from.plusMonths(1).minusDays(1)
        weeklyStatistic(from.toDate(), to.toDate())
    }

    @RequestMapping('/statistic/monthly/golongan/{month}/{year}')
    def monthlyStatisticPerGolongan(@PathVariable int month, @PathVariable int year) {
        def from = new DateTime().withMonthOfYear(month).withYear(year).withDayOfMonth(1)
        def to = from.plusMonths(1).minusDays(1)
        weeklyStatisticPerGolongan(from.toDate(), to.toDate())
    }

    @RequestMapping(method = RequestMethod.GET)
    WimEntity[] list() { wimRepository.findAll() }

    @RequestMapping(value = '/{e}', method = RequestMethod.GET)
    WimEntity find(@PathVariable WimEntity e) { e }

    @RequestMapping('/delete/{e}')
    WimEntity delete(@PathVariable WimEntity e) {
        wimRepository.delete(e); cameraService.deleteSnapshotOverload(e.snapshotFileName);
    }

    @RequestMapping(method = RequestMethod.POST)
    WimEntity save(@RequestBody WimEntity wimEntity) { wimRepository.save(wimEntity) }

    def laneid = ['AL', 'AR', 'AM', 'BL', 'BR', 'BM']

    @RequestMapping('/random')
    void generateData() {
        def random = new Random()
        def dateTime = new DateTime().minusMonths(8);
        dateTime = dateTime.plusMinutes(1 + random.nextInt(3))
        println dateTime
        def wimEntity = new WimEntity(measurementTime: dateTime.toDate().toTimestamp())
        wimEntity.laneId = laneid[random.nextInt(6)]
        def totalAxle = 2 + random.nextInt(6)
        (1..totalAxle).each {
            if (it != totalAxle)
                wimEntity."distAxleL$it" = wimEntity."distAxleR$it" = 1 + random.nextInt(5)
            wimEntity."axleWeight$it" = (5 + random.nextInt(17)) * 1000
        }
        wimEntity.speed = 1 + random.nextInt(100)
        wimEntity.snapshotFileName = new Date().getTime() + '.jpg'
        if (wimEntity.laneId[0] == 'A') {
            cameraService.snapshot('http://192.168.1.20/cgi-bin/viewer/video.jpg?streamid=1', wimEntity.snapshotFileName)
        } else {
            cameraService.snapshot('http://192.168.1.21/cgi-bin/viewer/video.jpg?streamid=1', wimEntity.snapshotFileName)
        }
        wimEntity.calculateOverweight(new ConfigEntity())
        wimRepository.save(wimEntity)
    }

    @RequestMapping('/export')
    void chart(HttpServletRequest request, HttpServletResponse response) {
        def transcoder = new JPEGTranscoder()
        transcoder.transcode(new TranscoderInput(new StringReader(request.getParameter('svg'))), new TranscoderOutput(response.getOutputStream()))
    }

    @RequestMapping('/last/{lane}')
    WimEntity getLast(@PathVariable String lane) {
        try {
            wimRepository.findAll(new Specification<WimEntity>() {
                @Override
                Predicate toPredicate(Root<WimEntity> wimEntityRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                    return criteriaBuilder.and(
                            criteriaBuilder.equal(wimEntityRoot.get(WimEntity_.laneId), lane),
                            criteriaBuilder.like(wimEntityRoot.get(WimEntity_.snapshotFileName), '%.jpg'));
                }
            }, new PageRequest(0, 1, new Sort(Sort.Direction.DESC, 'id'))).first()
        } catch (NoSuchElementException e) {
            return null;
        }
    }

}
