package com.sin.ubs.wim.core.config

import org.apache.tomcat.jdbc.pool.DataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
import org.springframework.context.annotation.*
import org.springframework.core.env.Environment
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.web.config.EnableSpringDataWebSupport
import org.springframework.http.converter.BufferedImageHttpMessageConverter
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.Database
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.client.RestTemplate
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import org.springframework.web.multipart.commons.CommonsMultipartResolver
import org.springframework.web.servlet.DispatcherServlet
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import org.springframework.web.servlet.view.InternalResourceViewResolver
import org.springframework.web.servlet.view.JstlView
import org.springframework.web.servlet.view.ResourceBundleViewResolver

import javax.servlet.annotation.WebServlet
import java.util.concurrent.Executor
import java.util.concurrent.Executors

@Configuration
@EnableWebMvc
@EnableLoadTimeWeaving
@EnableJpaRepositories('com.sin.ubs.wim.core.repository')
@EnableSpringDataWebSupport
@ComponentScan('com.sin.ubs.wim.core')
@EnableScheduling
@EnableAsync(mode = AdviceMode.ASPECTJ)
@PropertySource('classpath:config.properties')
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
class AppConfig extends WebMvcConfigurerAdapter implements SchedulingConfigurer, AsyncConfigurer {

    @Autowired
    Environment environment;

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate().with {
            getMessageConverters().add(new BufferedImageHttpMessageConverter())
            it
        }
    }

    @Bean(destroyMethod = 'close')
    javax.sql.DataSource dataSource() {
        new DataSource().with {
            setUrl(environment.getProperty('database.url'))
            setDriverClassName(environment.getProperty('database.driverClassName'))
            setUsername(environment.getProperty('database.username'))
            setPassword(environment.getProperty('database.password'))
            setTestOnBorrow(true)
            setTestOnConnect(true)
            setTestOnReturn(true)
            setValidationQuery("select 1")
            it
        };
    }

    @Bean
    JpaTransactionManager transactionManager() {
        new JpaTransactionManager()
    }

    @Bean(destroyMethod = 'destroy')
    LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        new LocalContainerEntityManagerFactoryBean().with {
            setDataSource(dataSource())
            setPackagesToScan('com.sin.ubs.wim.core.model.entity')
            setJpaVendorAdapter(new EclipseLinkJpaVendorAdapter().with {
                setDatabase(Database.POSTGRESQL)
                setShowSql(false)
                setGenerateDdl(true)
                it
            })
            it
        }
    }

    @Bean
    ResourceBundleViewResolver viewResolver() {
        return new ResourceBundleViewResolver(basename: 'view', order: 10)
    }

    @Bean
    InternalResourceViewResolver viewResolver2() {
        new InternalResourceViewResolver(prefix: '/WEB-INF/jsp/', suffix: '.jsp', viewClass: JstlView.class, order: 1000)
    }

    @Bean
    CommonsMultipartResolver multipartResolver() {
        new CommonsMultipartResolver();
    }

    @Bean
    ApplicationContextProvider applicationContext() {
        return new ApplicationContextProvider();
    }

    @Override
    void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(42))
    }

    @Override
    Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(7);
        executor.setMaxPoolSize(42);
        executor.setQueueCapacity(11);
        executor.setThreadNamePrefix("MyExecutor-");
        executor.setDaemon(true)
        executor.setWaitForTasksToCompleteOnShutdown(false)
        executor.initialize();
        return executor
    }

    @Override
    void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry)
    }

    @WebServlet(value = "/core/*", asyncSupported = true, loadOnStartup = 1)
    static class SpringServlet extends DispatcherServlet {

        SpringServlet() {
            super(new AnnotationConfigWebApplicationContext().with {
                register(AppConfig.class)
                it
            })
        }
    }

    @Bean
    public static PropertyPlaceholderConfigurer propertyConfigurer() throws IOException {
        PropertyPlaceholderConfigurer props = new PropertyPlaceholderConfigurer();
        props.setLocations([new ClassPathResource("config.properties")] as Resource[]);
        return props;
    }

}