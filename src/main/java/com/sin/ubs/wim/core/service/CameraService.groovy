package com.sin.ubs.wim.core.service

import groovy.util.logging.Slf4j
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

import javax.imageio.ImageIO
import javax.servlet.ServletContext
import java.awt.image.BufferedImage
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

/**
 * Created by sin on 22/03/14.
 */
@Service
@Slf4j
class CameraService {

    @Autowired
    ServletContext servletContext;
    @Autowired
    RestTemplate restTemplate;

    @Async
    void snapshot(String cameraUrl, String filename) {
        BufferedImage image = restTemplate.getForObject(cameraUrl, BufferedImage.class);
        File snapshotFile = new File(servletContext.getRealPath("snapshot"), filename);
        try {
            ImageIO.write(image, "jpg", snapshotFile);
            log.debug("capture snapshot: " + snapshotFile.getAbsolutePath());
        } catch (IOException e) {
            log.warn("cannot save image:" + snapshotFile, e);
        }
    }

    File getSnapshot(String filename) {
        return new File(servletContext.getRealPath("snapshot"), filename)
    }

    void deleteSnapshotOverload(String filename) {
        new File(servletContext.getRealPath("snapshot-overload"), filename).delete()
    }

    @Scheduled(fixedDelay = 7200000l, initialDelay = 60000l)
    void deleteOldSnapshot() {
        Files.walkFileTree(Paths.get(servletContext.getRealPath('snapshot')), new SimpleFileVisitor<Path>() {
            @Override
            FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toFile().getName() != '.gitignore' && attrs.creationTime().toMillis() < new DateTime().minusMinutes(10).toDate().getTime())
                    try {
                        Files.delete(file)
                    } catch (Exception e) {
                    }
                return super.visitFile(file, attrs)
            }
        });
    }
}
