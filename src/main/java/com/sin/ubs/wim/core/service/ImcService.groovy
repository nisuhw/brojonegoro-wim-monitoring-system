package com.sin.ubs.wim.core.service

import com.sin.ubs.wim.core.model.entity.WimEntity
import com.sin.ubs.wim.core.repository.WimRepository
import groovy.util.logging.Slf4j
import org.apache.commons.io.FileExistsException
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.exception.ExceptionUtils
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.text.NumberFormat

/**
 * Created by monitoring on 3/23/14.
 */
@Service
@Slf4j
class ImcService {

    @Value('${imc.project}')
    String imcProject;
    @Value('${imc.temporary_raw}')
    String imcTmp;
    @Value('${imc.converter}')
    String imcConverterExecutablePath
    @Autowired
    WimRepository wimRepository
    @Autowired
    ConfigService configService
    @Autowired
    CameraService cameraService
    Map<Path, String> rawfile = [:]

    @PostConstruct
    void init() {
        try {
            //FileUtils.forceDelete(new File(imcTmp))
            FileUtils.forceMkdir(new File(imcTmp))
        } catch (Exception e) {
        }
        //Thread.start { scanRawFile() }
    }

    @Scheduled(fixedDelay = 10000l)
    void scanRawFile() {
        rawfile = [:]
        Map<WatchKey, Path> keys = [:]

        WatchService watcher = FileSystems.getDefault().newWatchService();

        def registerAll = { Path path ->
            log.trace('listen to ' + path)
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                        throws IOException {
                    WatchKey key = dir.register(watcher,
                            StandardWatchEventKinds.ENTRY_CREATE,
                            StandardWatchEventKinds.ENTRY_MODIFY);
                    keys.put(key, dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        }

        registerAll(Paths.get(imcProject))

        for (; ;) {
            // wait for key to be signalled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                System.err.println("WatchKey not recognized!!");
                continue;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                WatchEvent.Kind kind = event.kind();

                // TBD - provide example of how OVERFLOW event is handled
                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = event as WatchEvent<Path>;
                Path name = ev.context();
                Path child = dir.resolve(name);

                // if directory is created, and watching recursively, then
                // register it and its sub-directories
                def filename = child.toFile().getName()

                // print out event
                log.trace(String.format("%s: %s", event.kind().name(), child));

                if (kind == StandardWatchEventKinds.ENTRY_CREATE && Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS))
                    registerAll(child);
                else if (filename.startsWith('Results_') && filename.endsWith(".raw") && rawfile[child] == null) {
                    String snapshotFilename = new Date().time + '.jpg'
                    rawfile[child] = snapshotFilename
                    cameraService.snapshot("http://192.168.1.2${filename[8] == 'A' ? '1' : '0'}/cgi-bin/viewer/video.jpg?streamid=1", snapshotFilename)
                    log.trace("save wim data $child -> $snapshotFilename")
                }
            }

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

                // all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
    }

    @Scheduled(fixedDelay = 3000l, initialDelay = 3000l)
    void processRawFile() {
        log.trace('start scanning raw file')
        boolean flagEmpty = true
        Files.walkFileTree(Paths.get(imcProject), new SimpleFileVisitor<Path>() {
            @Override
            FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                if (dir.toFile().name.contains("-") || dir.toFile().name.contains('_'))
                    return super.preVisitDirectory(dir, attrs)
                return FileVisitResult.SKIP_SUBTREE
            }

            @Override
            FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                def realFile = file.toFile()
                if (realFile.name.startsWith('Measurement')) return super.visitFile(file, attrs)
                if (attrs.creationTime().toMillis() < new DateTime().minusSeconds(3).toDate().time && realFile.name.toLowerCase().endsWith(".raw")) {
                    rawfile[file] = rawfile[file] ?: System.nanoTime().toString()
                    def destination = new File(Paths.get(imcTmp).toFile(), rawfile[file])
                    try {
                        log.trace("move from $realFile to $destination")
                        FileUtils.moveFileToDirectory(realFile, destination, true)
                    } catch (FileExistsException e) {
                        log.warn('file exist ' + realFile + ' -> ' + destination)
                        FileUtils.forceDelete(realFile)
                    }
                    rawfile.remove(file)
                    flagEmpty = false
                } else if (attrs.creationTime().toMillis() < new DateTime().minusSeconds(60).toDate().time) {
                    FileUtils.forceDelete(file.toFile())
                    log.trace("force delete $file")
                }
                return super.visitFile(file, attrs)
            }

            @Override
            FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (dir.toFile().listFiles().length == 0) {
                    FileUtils.forceDelete(dir.toFile())
                    log.trace("delete empty dir $dir")
                }
                return super.postVisitDirectory(dir, exc)
            }
        })
        if (flagEmpty) {
            log.trace('no raw file found')
            return
        }
        log.trace('call imc converter')
        callImcConverter(imcTmp)
        Files.walkFileTree(Paths.get(imcTmp), new SimpleFileVisitor<Path>() {
            String snapshot

            @Override
            FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                snapshot = dir.toFile().name
                return super.preVisitDirectory(dir, attrs)
            }

            @Override
            FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                File ascFile = file.toFile()
                def filename = ascFile.name
                if (filename.endsWith('asc')) {
                    try {
                        List<String> lines = ascFile.readLines().collect {
                            it.split('\t')[1].trim()
                        }
                        def newWimData = new WimEntity(laneId: filename[8..9], snapshotFileName: snapshot)
                        def numberFormat = NumberFormat.getInstance()
                        (1..12).step(2).eachWithIndex { itm, idx ->
                            newWimData."distAxleL${idx + 1}" = numberFormat.parse(lines[itm])
                        }
                        (2..12).step(2).eachWithIndex { itm, idx ->
                            newWimData."distAxleR${idx + 1}" = numberFormat.parse(lines[itm])
                        }
                        (13..19).eachWithIndex { itm, idx ->
                            newWimData."axleWeight${idx + 1}" = numberFormat.parse(lines[itm])
                        }
                        newWimData.grossVehicleWeight = numberFormat.parse(lines[20])
                        newWimData.speed = numberFormat.parse(lines[21])
                        newWimData.measurementTime = new DateTime()
                                .withDayOfMonth(numberFormat.parse(lines[22]).intValue())
                                .withMonthOfYear(numberFormat.parse(lines[23]).intValue())
                                .withYear(numberFormat.parse(lines[24]).intValue())
                                .withHourOfDay(numberFormat.parse(lines[25]).intValue())
                                .withMinuteOfHour(numberFormat.parse(lines[26]).intValue())
                                .withSecondOfMinute(numberFormat.parse(lines[27]).intValue()).toDate().toTimestamp()
                        newWimData.acceleration = numberFormat.parse(lines[28])
                        newWimData.calculateOverweight(configService.config)
                        if (!(newWimData.grossVehicleWeight <= 200 || newWimData.totalAxle < 1 || newWimData.speed > 100 || newWimData.distAxle1 > 10 || newWimData.distAxle1 < 0.1)) {
                            if (newWimData.snapshotFileName.endsWith('.jpg') && newWimData.generallyOverload)
                                FileUtils.copyFileToDirectory(cameraService.getSnapshot(newWimData.snapshotFileName), new File(cameraService.servletContext.getRealPath('snapshot-overload')))
                            wimRepository.save(newWimData)
                        }
                        log.trace("processed asc $ascFile")
                    } catch (Exception e) {
                        log.warn('failed proccess file: ' + filename)
                        if (!ExceptionUtils.getRootCause(e) instanceof java.sql.SQLIntegrityConstraintViolationException) {
                            return super.visitFile(file, attrs)
                        }
                    }
                }

                FileUtils.forceDelete(ascFile)
                return super.visitFile(file, attrs)
            }

            @Override
            FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                try {
                    Files.delete(dir)
                } catch (Exception e) {
                }
                return super.postVisitDirectory(dir, exc)
            }
        }

        )
    }

    public void callImcConverter(String dir) {
        log.info("converting raw to asc in " + dir);
        ProcessBuilder processBuilder = new ProcessBuilder(imcConverterExecutablePath, dir);
        try {
            Process process = processBuilder.start();
            process.waitFor();
            process.destroy();
        } catch (IOException e) {
        } catch (InterruptedException e) {
        }
    }

}


