package com.sin.ubs.wim.core.model.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.PrePersist
import java.sql.Timestamp

/**
 * Created by sin on 1/25/14.
 */
@Entity
class WimEntity implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    String laneId;
    Timestamp measurementTime;
    String snapshotFileName;
    Timestamp insertTime;
    double distAxleL1;
    double distAxleL2;
    double distAxleL3;
    double distAxleL4;
    double distAxleL5;
    double distAxleL6;
    double distAxleR1;
    double distAxleR2;
    double distAxleR3;
    double distAxleR4;
    double distAxleR5;
    double distAxleR6;
    double axleWeight1;
    double axleWeight2;
    double axleWeight3;
    double axleWeight4;
    double axleWeight5;
    double axleWeight6;
    double axleWeight7;
    double speed;
    double distAxle1;
    double distAxle2;
    double distAxle3;
    double distAxle4;
    double distAxle5;
    double distAxle6;
    double acceleration;
    boolean axleOverload1;
    boolean axleOverload2;
    boolean axleOverload3;
    boolean axleOverload4;
    boolean axleOverload5;
    boolean axleOverload6;
    boolean axleOverload7;
    int totalAxle;
    boolean vehicleOverload;
    boolean generallyOverload;
    double grossVehicleWeight;
    String golongan;
    String vClass
    String remark;

    void calculateOverweight(ConfigEntity configEntity) {
        (1..7).each {
            if (it != 7)
                this."distAxle$it" = (this."distAxleL$it" + this."distAxleR$it") / 2
            if (this."axleWeight$it" > 0.0f) totalAxle++
        }
        vClass = ''
        remark = ''
        if (distAxle1 < 3 && totalAxle == 2) {
            golongan = 'I'
            vClass = grossVehicleWeight < 900 ? 'a' : 'b'
            axleOverload1 = axleWeight1 > configEntity.limitFW
            if (axleOverload1) remark += 'W1 '
            axleOverload2 = axleWeight2 > configEntity.limitFW
            if (axleOverload2) remark += 'W2 '
            vehicleOverload = grossVehicleWeight > configEntity.limitFW * 2
        } else {
            golongan = totalAxle == 2 ? 'II' : totalAxle == 3 ? 'III' : totalAxle == 4 ? 'IV' : totalAxle == 5 ? 'V' : totalAxle == 6 ? 'VI' : totalAxle == 7 ? 'VII' : '-'
            def maxGVW = 0
            for (int i = 1; i <= totalAxle;) {
                if (i + 2 <= totalAxle && this."distAxle$i" + this."distAxle${i + 1}" < 3.7) {
                    this."axleOverload$i" = this."axleOverload${i + 1}" = this."axleOverload${i + 2}" =
                            this."axleWeight$i" + this."axleWeight${i + 1}" + this."axleWeight${i + 2}" > configEntity.limitTridem
                    if (this."axleOverload$i") remark += "W$i${i + 1}${i + 2} "
                    maxGVW += configEntity.limitTridem
                    i += 3
                    vClass += '3'
                } else if (i + 1 <= totalAxle && this."distAxle$i" < 1.5) {
                    this."axleOverload$i" = this."axleOverload${i + 1}" =
                            this."axleWeight$i" + this."axleWeight${i + 1}" > configEntity.limitTandem
                    if (this."axleOverload$i") remark += "W$i${i + 1} "
                    maxGVW += configEntity.limitTandem
                    i += 2
                    vClass += '2'
                } else {
                    def limit = (i == 1 ? configEntity.limitFW : configEntity.limitBW)
                    this."axleOverload$i" = this."axleWeight$i" > limit;
                    if (this."axleOverload$i") remark += "W$i "
                    maxGVW += limit
                    i++
                    vClass += '1'
                }
            }
            vehicleOverload = grossVehicleWeight > maxGVW
        }
        if (vehicleOverload) remark += 'GVW '
        generallyOverload = !remark.isEmpty()
    }

    @PrePersist
    void prePersist() {
        insertTime = new Date().toTimestamp()
        remark = remark ?: '-';
    }

}