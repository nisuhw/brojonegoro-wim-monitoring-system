package com.sin.ubs.wim.core.controller

import com.sin.ubs.wim.core.model.entity.WimEntity
import com.sin.ubs.wim.core.model.entity.WimEntity_
import com.sin.ubs.wim.core.repository.WimRepository
import com.sin.ubs.wim.core.service.CameraService
import org.apache.commons.lang3.time.DateUtils
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.Specification
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

/**
 * Created by monitoring on 3/25/14.
 */
@Controller
@RequestMapping('/data')
class DataController {

    @Autowired
    private WimRepository wimRepository
    @Autowired
    private CameraService cameraService;

    @RequestMapping('/delete/{e}')
    String delete(@PathVariable WimEntity e) {
        wimRepository.delete(e.id); cameraService.deleteSnapshotOverload(e.snapshotFileName);
        "redirect:/core/data/list/overload"
    }

    @RequestMapping('/list/overload')
    String getOverloadTruck(
            @RequestParam(required = false) @DateTimeFormat(pattern = 'MMM yyyy') Date period, Model model) {
        period = new DateTime(period ?: new Date()).withDayOfMonth(1).toDate().clearTime()
        model.addAttribute('wimList', wimRepository.findAll(new Specification<WimEntity>() {
            @Override
            Predicate toPredicate(Root<WimEntity> wimEntityRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                criteriaBuilder.and(
                        criteriaBuilder.greaterThanOrEqualTo(wimEntityRoot.get(WimEntity_.measurementTime), period.clearTime()),
                        criteriaBuilder.lessThan(wimEntityRoot.get(WimEntity_.measurementTime), new DateTime(period.clearTime()).plusMonths(1).toDate()),
                        criteriaBuilder.isTrue(wimEntityRoot.get(WimEntity_.generallyOverload)),
                        criteriaBuilder.like(wimEntityRoot.get(WimEntity_.snapshotFileName), '%.jpg'))
            }
        }).sort { it.measurementTime })
        def periodList = []
        12.times {
            periodList << DateUtils.addMonths(new Date(), -1 * it).format('MMM yyyy')
        }
        model.addAttribute('periodList', periodList)
        "overload"
    }
}
