package com.sin.ubs.wim.core.model.entity

import javax.persistence.*

/**
 * Created by sin on 2/18/14.
 */
@Entity
@Cacheable(false)
class ConfigEntity implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    double limitFW = 6_000
    double limitBW = 10_000
    double limitTandem = 18_000
    double limitTridem = 21_000

    @Temporal(TemporalType.DATE)
    Date expiredDemo
    String key

    @PrePersist
    @PreUpdate
    void checkKey() {
        if (key == null) key = "";
    }
}
