package com.sin.ubs.wim.core.service

import com.sin.ubs.wim.core.repository.WimRepository
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.time.DateUtils
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

/**
 * Created by sin on 1/25/14.
 */
@Service
@Slf4j
class WimService {

    @Autowired
    WimRepository wimRepository
    def wimListeners = []
    def lastWimId = -1;

    @Scheduled(fixedDelay = 1000l)
    void wimDbMonitor() {
        log.info('start wim db monitor service')
        try {
            for (; ;) {
                def lastWimIdFromDb = wimRepository.getLastWimId() ?: -1;
                if (lastWimIdFromDb > lastWimId) {
                    log.debug('detect new wim data')
                    wimListeners.each {
                        try {
                            it.process(lastWimIdFromDb)
                        } catch (Exception e) {
                            wimListeners.remove(it)
                            it.close()
                        }
                    }
                    lastWimId = lastWimIdFromDb
                }
                Thread.sleep(1000)
            }
        } finally {
            wimListeners.each { it.close() }
            wimListeners.clear()
        }
    }

    def wimDataPerHour(Date today, boolean overload) {
        def perHour = []
        24.times {
            perHour << wimRepository.countWimData(today, it, overload)
        }
        perHour
    }

    def wimDataPerHourPerGolongan(Date today, boolean overload) {
        def result = [:]
        ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'].each { gol ->
            def perHour = []
            24.times {
                perHour << wimRepository.countWimDataPerGolongan(today, it, overload, gol)
            }
            result[gol] = perHour
        }
        result
    }

    def wimDataPerDay(Date from, Date to, boolean overload) {
        def result = []
        while (!from.after(to)) {
            result << wimRepository.countWimData(from, overload)
            from = DateUtils.addDays(from, 1);
        }
        result
    }

    def wimDataPerDayPerGolongan(Date from, Date to, boolean overload) {
        def map = [:]
        ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'].each { gol ->
            def result = []
            while (!from.after(to)) {
                result << wimRepository.countWimData(from, overload, gol)
                from = DateUtils.addDays(from, 1);
            }
            map[gol] = result
        }
        map
    }

    def wimDataPerMonth(int month, int year, boolean overload) {
        def from = new DateTime().withYear(year).withMonthOfYear(month).withDayOfMonth(1);
        def to = from.plusMonths(1).minusDays(1)
        wimDataPerDay(from.toDate(), to.toDate(), overload)
    }

    def wimDataPerMonthPerGolongan(int month, int year, boolean overload) {
        def from = new DateTime().withYear(year).withMonthOfYear(month).withDayOfMonth(1);
        def to = from.plusMonths(1).minusDays(1)
        wimDataPerDayPerGolongan(from.toDate(), to.toDate(), overload)
    }

}
