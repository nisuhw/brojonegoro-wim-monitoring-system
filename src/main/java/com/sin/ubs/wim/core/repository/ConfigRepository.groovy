package com.sin.ubs.wim.core.repository

import com.sin.ubs.wim.core.model.entity.ConfigEntity
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by sin on 1/25/14.
 */

interface ConfigRepository extends JpaRepository<ConfigEntity, Long> {

}
