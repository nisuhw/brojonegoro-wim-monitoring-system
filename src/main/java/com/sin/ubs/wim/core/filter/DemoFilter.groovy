package com.sin.ubs.wim.core.filter

import com.sin.ubs.wim.core.config.ApplicationContextProvider
import com.sin.ubs.wim.core.service.ConfigService

import javax.servlet.*
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by sin on 5/23/2014.
 */
@WebFilter(asyncSupported = true, urlPatterns = '/dashboard')
class DemoFilter implements Filter {

    @Override
    void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletrequest = (request as HttpServletRequest)
        HttpServletResponse httpServletResponse = (response as HttpServletResponse)
        if (httpServletrequest.getParameter("skipFilter") != null) {
            chain.doFilter(request, response)
        }
        ConfigService configService = ApplicationContextProvider.getBean(ConfigService.class)
        if (configService.config.expiredDemo != null) {
            httpServletResponse.sendRedirect("/core/demo")
        } else {
            chain.doFilter(request, response)
        }
    }

    @Override
    void destroy() {

    }
}
