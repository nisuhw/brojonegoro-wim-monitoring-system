package com.sin.ubs.wim.core.model

import com.sin.ubs.wim.core.model.entity.WimEntity

/**
 * Created by sin on 3/17/14.
 */
class SummaryReportModel implements Comparable<SummaryReportModel> {
    Date date;
    long overload1
    long overload2
    long overload3
    long overload4
    long overload5
    long overload6
    long overload7
    long totalVehicle;

    def plus(WimEntity w) {
        if (w.vehicleOverload) {
            ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'].eachWithIndex { it, i ->
                if (w.golongan == it) {
                    this."overload${i + 1}"++
                }
            }
        }
        totalVehicle++
        this
    }

    @Override
    int compareTo(SummaryReportModel o) {
        return this.date.compareTo(o.date)
    }
}
