package com.sin.ubs.wim.core.repository

import com.sin.ubs.wim.core.model.entity.WimEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query

/**
 * Created by sin on 1/25/14.
 */

interface WimRepository extends JpaRepository<WimEntity, Long>, JpaSpecificationExecutor<WimEntity> {

    @Query('select max(e.id) from WimEntity e')
    Long getLastWimId();

    @Query(nativeQuery = true, value = "select * from WimEntity e where date(measurementtime) between ?1 and ?2")
    List<WimEntity> findByMeasurementtime(Date from, Date to)

    @Query(nativeQuery = true, value = "select * from WimEntity e where date(measurementtime) between ?1 and ?2 and e.vehicleOverload=true")
    List<WimEntity> findByMeasurementtimeOverload(Date from, Date to)

    @Query(nativeQuery = true, value = "select count(w) from wimentity w where date(measurementtime)=?1 and date_part('hour',measurementtime)=?2 and vehicleoverload=?3")
    Long countWimData(Date date, int hour, boolean overload)

    @Query(nativeQuery = true, value = "select count(w) from wimentity w where date(measurementtime)=?1 and date_part('hour',measurementtime)=?2 and vehicleoverload=?3 and golongan=?4")
    Long countWimDataPerGolongan(Date date, int hour, boolean overload, String golongan)

    @Query(nativeQuery = true, value = "select count(w) from wimentity w where date(measurementtime)=?1 and vehicleoverload=?2")
    Long countWimData(Date day, boolean overload)

    @Query(nativeQuery = true, value = "select count(w) from wimentity w where date(measurementtime)=?1 and vehicleoverload=?2 and golongan=?3")
    Long countWimData(Date day, boolean overload, String golongan)

}
