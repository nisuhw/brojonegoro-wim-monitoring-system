package com.sin.ubs.wim.core.service

import com.sin.ubs.wim.core.model.entity.ConfigEntity
import com.sin.ubs.wim.core.repository.ConfigRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by monitoring on 3/25/14.
 */
@Service
class ConfigService {
    @Autowired
    private ConfigRepository configRepository

    ConfigEntity getConfig() {
        def page = configRepository.findAll(new PageRequest(0, 1, Sort.Direction.DESC, 'id'))
        page.hasContent() ? page.first() : new ConfigEntity()
    }

    @Transactional
    void updateConfig(ConfigEntity configEntity) {
        configRepository.save(configEntity)
    }
}
