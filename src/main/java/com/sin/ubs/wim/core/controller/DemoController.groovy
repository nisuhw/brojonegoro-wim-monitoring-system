package com.sin.ubs.wim.core.controller

import com.sin.ubs.wim.core.service.ConfigService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by sin on 5/23/2014.
 */
@Controller
@RequestMapping("/demo")
class DemoController {
    @Autowired
    private ConfigService configService

    @RequestMapping
    String gotoConfirmPage(Model model) {
        def config = configService.config
        model.addAttribute('expiredDemo', config.expiredDemo.format('dd-MM-yyyy'))
        model.addAttribute('lanjut', config.expiredDemo > new Date())
        "demo/confirm"
    }

    @RequestMapping('/iframe')
    String iframePage(Model model) {
        def config = configService.config
        if (config.expiredDemo <= new Date()) {
            "redirect:/core/demo"
        } else {
            model.addAttribute('expiredDemo', config.expiredDemo.format('dd-MM-yyyy'))
            "demo/iframe"
        }
    }

    @RequestMapping(value = "/register")
    @ResponseBody
    boolean getRegisterPage(@RequestParam String key) {
        def config = configService.config
        if (config.key.equals(key)) {
            config.expiredDemo = null
            configService.updateConfig(config)
            true
        } else {
            false
        }
    }
}
