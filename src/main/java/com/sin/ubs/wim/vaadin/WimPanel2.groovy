package com.sin.ubs.wim.vaadin

import com.vaadin.server.ExternalResource
import com.vaadin.ui.*

/**
 * Created by sin on 3/16/14.
 */
class WimPanel2 extends WimPanel {
    WimPanel2(String title, boolean swap) {
        initComponent(title, swap)
    }

    VerticalLayout snapshotLayout
    def streaming

    void hideCamera() {
        streaming = snapshotLayout.getComponent(0)
        snapshotLayout.removeComponent(streaming)
    }

    void showCamera() {
        snapshotLayout.addComponent(streaming, 0)
    }

    protected void initComponent(String title, boolean swap) {
        def rootLayout = new HorizontalLayout().with {
            setDefaultComponentAlignment(Alignment.MIDDLE_CENTER)
            def cameraSetting = { BrowserFrame p1 -> p1.setWidth('415px'); p1.setHeight('240px'); p1; }
            if (swap)
                addComponent snapshotLayout = new VerticalLayout(new BrowserFrame('', new ExternalResource('/tongawi.html')).with(cameraSetting), snapshot)
            addComponent new VerticalLayout().with {
                setDefaultComponentAlignment(Alignment.MIDDLE_CENTER)
                setWidth("228px")
                addStyleName("wim-panel")
                addComponent new Label(title).with {
                    setSizeUndefined()
                    addStyleName("title")
                    it
                }
                addComponents([
                        ["Record", recordNo],
                        ["Golongan", golongan],
                        ["Lane", lane],
                        ["Date", date],
                        ["Time", time],
                        ["Vehicle Weight", grossWeight],
                        ["Speed", speed]
                ].collect {
                    it[1].addStyleName("info-column-value")
                    def layout = new HorizontalLayout(new Label(it[0]), it[1])
                    layout.addStyleName("info-column")
                    layout.setSizeFull()
                    layout
                } as Component[])
                addComponent(new HorizontalLayout().with {

                    addComponent(new VerticalLayout().with { v ->
                        v.addComponent(new Label(value: '#', styleName: 'header number'))
                        (1..7).each { v.addComponent(new Label(value: it.toString(), styleName: 'number')) }
                        v.width = '30px'
                        v
                    })
                    addComponent(new VerticalLayout().with { v ->
                        v.addComponent(new Label(value: 'Axle Weight', styleName: 'header'))
                        (1..7).each { v.addComponent(this."axW$it"); this."axW$it".styleName = 'content' }
                        v.setSizeFull()
                        v
                    })
                    addComponent(new VerticalLayout().with { v ->
                        v.addComponent(new Label(value: 'Axle Dist', styleName: 'header'))
                        (1..7).each { v.addComponent(this."axD$it"); this."axD$it".styleName = 'content' }
                        v.setSizeFull()
                        v
                    })
                    setSizeFull()
                    addStyleName('info-axle')
                    setExpandRatio(it.getComponent(1), 1f)
                    setExpandRatio(it.getComponent(2), 1f)
                    it
                })
                addComponent pauseBtn
                it
            }
            if (!swap)
                addComponent snapshotLayout = new VerticalLayout(new BrowserFrame('', new ExternalResource('/tocepu.html')).with(cameraSetting), snapshot)
            it
        }
        setCompositionRoot(rootLayout)
        addStyleName("wim-panel-wrapper")
        setSizeFull()
    }
}