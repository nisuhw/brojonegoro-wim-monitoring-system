package com.sin.ubs.wim.vaadin

import com.vaadin.ui.Alignment
import com.vaadin.ui.FormLayout
import com.vaadin.ui.VerticalLayout
import com.vaadin.ui.Window

/**
 * Created by sin on 3/14/14.
 */
class FormWindow extends Window {

    FormWindow(String width, String height, String caption, WimPanel2 panel1, WimPanel2 panel2, FormLayout formLayout) {
        setWidth(width)
        setHeight(height)
        center()
        this.caption = caption
        content = new VerticalLayout()
        content.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER)
        content.setSizeFull()
        content.addComponent(formLayout)
        formLayout.setSizeUndefined()
        formLayout.spacing = true
        panel1.hideCamera()
        panel2.hideCamera()
        addCloseListener({ panel1.showCamera(); panel2.showCamera(); })
    }
}
