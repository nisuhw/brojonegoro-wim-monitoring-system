package com.sin.ubs.wim.vaadin

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.model.*
import com.vaadin.ui.VerticalLayout
import com.vaadin.ui.Window

/**
 * Created by sin on 3/14/14.
 */
class StatisticWindow extends Window {
    StatisticWindow(String title, data) {
        setSizeFull()
        resizable = false
        center()
        content = new VerticalLayout(new Chart(ChartType.COLUMN).with {
            Configuration conf = getConfiguration();
            conf.setTitle(new Title(title));

            conf.addyAxis(new YAxis().with {
                allowDecimals = false
                min = 0
                setTitle('# vehicle')
                it;
            })

            conf.addxAxis(new XAxis().identity {
                categories = data.cat as String[]
                it
            });

            conf.plotOptions = new PlotOptionsColumn(stacking: Stacking.NORMAL, groupPadding: 0.1, pointPadding: 0.1)

            data.serie.each {
                ListSeries serie = new ListSeries(it.data.label, it.data.value as Number[]);
                serie.setStack(it.stack.toString());
                conf.addSeries(serie);
            }
            conf.setExporting(new Exporting(true).with {
                setUrl('/core/wim/export')
                it
            })
            conf.legend = new Legend()

            setSizeFull()
            it
        }).with {
            setSizeFull()
            it
        }
    }
}
