package com.sin.ubs.wim.vaadin

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.model.*

/**
 * Created by sin on 3/10/14.
 */
class DailyStatisticChart extends Chart {
    DailyStatisticChart(Date date, data) {
        super(ChartType.COLUMN)
        Configuration conf = getConfiguration();
        conf.setTitle(new Title("Summary of vehicle at " + date.format('yyyy/MM/dd')));

        conf.addyAxis(new YAxis().with {
            allowDecimals = false
            min = 0
            setTitle('# vehicle')
            it;
        })

        conf.addxAxis(new XAxis().identity {
            categories = [].identity { cat ->
                24.times {
                    def jam = String.format('%02d', it)
                    cat.add(jam + ':00:00<br/>-<br/>' + jam + ':59:59')
                };
                cat;
            } as String[]
            it
        });

        conf.plotOptions = new PlotOptionsColumn(stacking: Stacking.NORMAL, groupPadding: 0.1, pointPadding: 0.1)

        data.each {
            ListSeries serie = new ListSeries(it.data.label, it.data.value as Number[]);
            serie.setStack(it.stack);
            conf.addSeries(serie);
        }
    }
}
