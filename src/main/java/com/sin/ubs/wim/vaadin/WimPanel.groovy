package com.sin.ubs.wim.vaadin

import com.sin.ubs.wim.core.model.entity.WimEntity
import com.vaadin.server.ExternalResource
import com.vaadin.ui.*

import java.text.DecimalFormat

/**
 * Created by sin on 1/6/14.
 */
class WimPanel extends CustomComponent {
    def snapshot = new Image().with {
        addStyleName('snapshot')
        setWidth('380px'); setHeight('214px');
        it;
    }
    def golongan = new Label("-")
    def lane = new Label("-")
    def recordNo = new Label("-")
    def date = new Label("-")
    def time = new Label('-')
    def grossWeight = new Label('-')
    def speed = new Label('-')
    def axW1 = new Label('-')
    def axW2 = new Label('-')
    def axW3 = new Label('-')
    def axW4 = new Label('-')
    def axW5 = new Label('-')
    def axW6 = new Label('-')
    def axW7 = new Label('-')
    def axD1 = new Label('-')
    def axD2 = new Label('-')
    def axD3 = new Label('-')
    def axD4 = new Label('-')
    def axD5 = new Label('-')
    def axD6 = new Label('-')
    def axD7 = new Label('-')
    def pauseBtn = new CheckBox("Pause").with {
        addStyleName("toggle-btn")
        it
    }

    WimPanel() {}

    WimPanel(String title) {
        initComponent(title)
    }

    protected void initComponent(String title) {
        def rootLayout = new VerticalLayout()
        rootLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER)
        rootLayout.setWidth("228px")
        rootLayout.addStyleName("wim-panel")
        rootLayout.addComponent snapshot
        def titlelabel = new Label(title)
        titlelabel.setSizeUndefined()
        titlelabel.addStyleName("title")
        rootLayout.addComponent titlelabel
        rootLayout.addComponents([
                ["Record", recordNo],
                ["Golongan", golongan],
                ["Date", date],
                ["Time", time],
                ["Vehicle Weight", grossWeight],
                ["Speed", speed]
        ].collect {
            it[1].addStyleName("info-column-value")
            def layout = new HorizontalLayout(new Label(it[0]), it[1])
            layout.addStyleName("info-column")
            layout.setSizeFull()
            layout
        } as Component[])

        def grid = new GridLayout(3, 8)
        grid.addStyleName('info-grid')

        rootLayout.addComponent(new HorizontalLayout().with {
            it.addComponent(new VerticalLayout().with { v ->
                v.addComponent(new Label(value: '#', styleName: 'header number'))
                (1..7).each { v.addComponent(new Label(value: it.toString(), styleName: 'number')) }
                v.width = '30px'
                v
            })
            it.addComponent(new VerticalLayout().with { v ->
                v.addComponent(new Label(value: 'Axle Weight', styleName: 'header'))
                (1..7).each { v.addComponent(this."axW$it"); this."axW$it".styleName = 'content' }
                v.setSizeFull()
                v
            })
            it.addComponent(new VerticalLayout().with { v ->
                v.addComponent(new Label(value: 'Axle Dist', styleName: 'header'))
                (1..7).each { v.addComponent(this."axD$it"); this."axD$it".styleName = 'content' }
                v.setSizeFull()
                v
            })
            it.setSizeFull()
            it.addStyleName('info-axle')
            it.setExpandRatio(it.getComponent(1), 1f)
            it.setExpandRatio(it.getComponent(2), 1f)
            it
        })

        pauseBtn.addStyleName("toggle-btn")
        rootLayout.addComponent(pauseBtn)

        setCompositionRoot(rootLayout)
        addStyleName("wim-panel-wrapper")
        setSizeFull()
    }

    void update(WimEntity wimEntity) {
        if (wimEntity == null) return;
        if (!wimEntity.snapshotFileName.endsWith('.jpg')) return
        def format = new DecimalFormat('#.00')
        if (pauseBtn.getState().checked) return;
        snapshot.source = new ExternalResource("/snapshot/" + wimEntity.snapshotFileName)
        golongan.value = wimEntity.golongan
        recordNo.value = wimEntity.id
        lane.value = wimEntity.laneId
        date.value = wimEntity.measurementTime.format('yyyy-MM-dd')
        time.value = wimEntity.measurementTime.format('HH:mm:ss')
        grossWeight.value = format.format(wimEntity.grossVehicleWeight)
        if (wimEntity.vehicleOverload)
            grossWeight.addStyleName('overload')
        else
            grossWeight.removeStyleName('overload')
        speed.value = wimEntity.speed
        (1..7).each {
            if (wimEntity."axleWeight$it" == 0.0)
                this."axW$it".value = '-'
            else
                this."axW$it".value = format.format(wimEntity."axleWeight$it")
            if (it != 7)
                if (wimEntity."distAxle$it" == 0.0)
                    this."axD$it".value = '-'
                else
                    this."axD$it".value = format.format(wimEntity."distAxle$it")
            if (wimEntity."axleOverload$it")
                this."axW$it".addStyleName('overload')
            else
                this."axW$it".removeStyleName('overload')

        }
    }
}
