package com.sin.ubs.wim.vaadin

import com.sin.ubs.wim.core.config.ApplicationContextProvider
import com.sin.ubs.wim.core.service.WimService
import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.server.ExternalResource
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.*
import groovy.json.JsonSlurper
import org.apache.commons.lang3.time.DateUtils
import org.springframework.web.client.RestTemplate

@Theme("mytheme")
@SuppressWarnings("serial")
@Push
class MainUI extends UI {
    private RestTemplate restTemplate = ApplicationContextProvider.getBean(RestTemplate.class)
    def toNgawi = new WimPanel2("To Ngawi", true)
    def toCepu = new WimPanel2("To Cepu", false)

    void monitorWimData() {
        WimService wimService = ApplicationContextProvider.applicationContext.getBean(WimService.class)
        boolean start = false
        while (true) {
            if (!start) {
                start = true;
                wimService.wimListeners << [process: { data ->
                    def myself = this;
                    Thread.startDaemon {
                        try {
                            super.access {
                                def wimEntity = wimService.wimRepository.findOne(data)
                                if (wimEntity.laneId[0] == 'A')
                                    toNgawi.update(wimEntity)
                                else
                                    toCepu.update(wimEntity)
                            }
                        } catch (UIDetachedException e) {
                            wimService.wimListeners.remove(myself)
                        }
                    }
                }, close                           : { start = false }]
            }
            Thread.sleep(10000)
        }
    }

    @Override
    protected void init(VaadinRequest request) {
        MenuBar menuBar = new MenuBar(width: "100%").with {
            addItem("File", null).with {
                addItem("Refresh", { JavaScript.getCurrent().execute("window.location.reload()") })
                addItem("Exit", new MenuBar.Command() {
                    @Override
                    void menuSelected(MenuBar.MenuItem selectedItem) {
                        close()
                        JavaScript.getCurrent().execute("window.close()")
                    }
                })
            }
            addItem("View", null).with {
                addItem("Camera", null).with {
                    addItem("Camera 1", { JavaScript.getCurrent().execute("window.location='/tongawi-full.html'") })
                    addItem("Camera 2", { JavaScript.getCurrent().execute("window.location='/tocepu-full.html'") })
                }
                addItem("Statistics", null).with {
                    addItem('Daily Vehicle Statistic', {
                        super.addWindow(new FormWindow('290px', '254px', 'Daily Statistic', toNgawi, toCepu, new FormLayout().with { form ->
                            DateField date
                            CheckBox group
                            addComponent(date = new PopupDateField(caption: 'Select Day', dateFormat: 'yyyy/MM/dd', width: '122px', required: true, requiredError: 'The Field may not be empty.'))
                            addComponent(group = new CheckBox('Group by golongan'))
                            addComponent(new Button('Submit', {
                                if (date.value == null) return;
                                def chartData = [cat: []]
                                24.times {
                                    def jam = String.format('%02d', it)
                                    chartData.cat.add(jam + ':00:00<br/>-<br/>' + jam + ':59:59')
                                };

                                if (group.value) {
                                    def result = new JsonSlurper().parseText(restTemplate.getForObject('http://localhost:8080/core/wim/statistic/daily/golongan/' + date.value.format('yyyy-MM-dd'), String.class))
                                    chartData.serie = []
                                    ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'].eachWithIndex { itm, idx ->
                                        chartData.serie << [stack: idx, data: [label: "Gol $itm", value: result[itm].normal]]
                                        chartData.serie << [stack: idx, data: [label: "Gol $itm Overload", value: result[itm].overload]]
                                    }

                                } else {
                                    def result = new JsonSlurper().parseText(restTemplate.getForObject('http://localhost:8080/core/wim/statistic/daily/' + date.value.format('yyyy-MM-dd'), String.class))
                                    chartData.serie = [
                                            [stack: '1', data: [label: 'Normal Vehicle', value: result.normal]],
                                            [stack: '1', data: [label: 'Overload Vehicle', value: result.overload]],
                                    ]
                                }
                                super.addWindow(new StatisticWindow("Summary of vehicle at " + date.value.format('yyyy/MM/dd'), chartData))
                            } as Button.ClickListener))
                            form
                        }))
                    } as MenuBar.Command)
                    addItem('Weekly Vehicle Statistic', {
                        super.addWindow(new FormWindow('290px', '300px', 'Weekly Statistic', toNgawi, toCepu, new FormLayout().with { form ->
                            DateField from, to
                            CheckBox group
                            addComponent(from = new PopupDateField(caption: 'Select Start Date', dateFormat: 'yyyy/MM/dd', width: '122px'))
                            addComponent(to = new PopupDateField(caption: 'Select End Date', dateFormat: 'yyyy/MM/dd', width: '122px'))
                            addComponent(group = new CheckBox('Group by golongan'))
                            addComponent(new Button('Submit', {
                                if (from.value == null || to.value == null) return;
                                generateWeeklyData(from.value, to.value, group.value)
                            } as Button.ClickListener))
                            form
                        }))
                    } as MenuBar.Command)
                    addItem('Monthly Vehicle Statistic', {
                        super.addWindow(new FormWindow('290px', '254px', 'Monthly Statistic', toNgawi, toCepu, new FormLayout().with { form ->
                            NativeSelect periode
                            CheckBox group
                            addComponent(periode = new NativeSelect('Periode').with {
                                12.times {
                                    def period = DateUtils.addMonths(new Date(), -1 * it)
                                    addItem(period)
                                    setItemCaption(period, period.format('MMM yyyy'))
                                }
                                it
                            })
                            addComponent(group = new CheckBox('Group by golongan'))
                            addComponent(new Button('Submit', {
                                if (periode.value == null) return;
                                Date date = DateUtils.setDays(periode.value as Date, 1)
                                generateWeeklyData(date, DateUtils.addMonths(date, 1) - 1, group.value)
                            } as Button.ClickListener))
                            form
                        }))
                    })
                }
                addItem('Report', {
                    super.addWindow(new FormWindow('290px', '300px', 'Report', toNgawi, toCepu, new FormLayout().with { form ->
                        NativeSelect periode
                        OptionGroup format, type
                        CheckBox overloadOnly
                        addComponent(periode = new NativeSelect('Periode').with {
                            12.times {
                                def period = DateUtils.addMonths(new Date(), -1 * it)
                                addItem(period)
                                setItemCaption(period, period.format('MMM yyyy'))
                            }
                            it
                        })
                        addComponent(format = new OptionGroup('Format').with { o ->
                            setStyleName('horizontal')
                            ['pdf', 'xls'].each {
                                o.addItem(it)
                            }
                            o
                        })
                        addComponent(type = new OptionGroup('Type').with { o ->
                            setStyleName('horizontal')
                            ['Summary', 'Detail'].each {
                                o.addItem(it)
                            }
                            o
                        })
                        addComponent(overloadOnly = new CheckBox('Data Overload Only').with { setValue(true); it; })
                        addComponent(new Button('Submit', {
                            if (periode.value == null || format.value == null || type.value == null) return;
                            Date from = DateUtils.setDays(periode.value as Date, 1)
                            Date to = DateUtils.addMonths(from, 1) - 1
                            new Notification('Report may be generated over an hour, please be patient').with {
                                setDelayMsec(5000); it;
                            }.show(super.getPage());
                            JavaScript.current.execute("window.location='/core/wim/report/$format.value/${from.format('yyyy-MM-dd')}/${to.format('yyyy-MM-dd')}/wim-${type.value.toString().toLowerCase()}.$format.value" + (overloadOnly.getValue() ? '?overloadOnly=true' : '') + "'")
                        } as Button.ClickListener))
                        form
                    }))
                })
            }
            addItem("Edit", null).with {
                addItem('Data Overload', {
                    JavaScript.current.execute("window.location='/core/data/list/overload'")
                })
                addItem('Overload Limit Config', {
                    super.addWindow(new ConfigWindow())
                })
            }
            addItem("Help", {
                ProcessBuilder processBuilder = new ProcessBuilder('C:\\Program Files (x86)\\Microsoft Office\\Office12\\WINWORD.EXE', 'D:\\Tools\\WIM-UserGuide.docx');
                try {
                    Process process = processBuilder.start();
                    process.waitFor();
                    process.destroy();
                } catch (Exception e) {
                    e.printStackTrace()
                }
            })
            it
        }

        def snapshot1 = new ExternalResource("/VAADIN/themes/mytheme/img/notruck.jpg")
        def snapshot2 = new ExternalResource("/VAADIN/themes/mytheme/img/notruck.png")

        toNgawi.snapshot.source = snapshot1
        toCepu.snapshot.source = snapshot2

        def rootLayout = new HorizontalLayout(toNgawi, toCepu)
        rootLayout.setSizeFull()
        rootLayout.addStyleName("root-panel")

        content = new VerticalLayout(menuBar, new HorizontalLayout().with {
            addStyleName('logo-header')
            setSizeFull()
            setHeight('50px')

            def pu = new Image('', new ExternalResource('/logo-pu.jpg')).with {
                setSizeUndefined(); setHeight('50px'); setWidth('50px'); it;
            }
            addComponent(pu)
            setComponentAlignment(pu, Alignment.MIDDLE_LEFT)

            def hk = new Image('', new ExternalResource('/logo-hk.jpg')).with {
                setSizeUndefined(); setHeight('50px'); setWidth('50px'); it;
            }
            addComponent(hk)
            setComponentAlignment(hk, Alignment.MIDDLE_CENTER)
            def spi = new Image('', new ExternalResource('/logo-spi.jpg')).with {
                setSizeUndefined(); setHeight('50px'); setWidth('50px'); it;
            }
            addComponent(spi)
            setComponentAlignment(spi, Alignment.MIDDLE_RIGHT)
            it
        }, rootLayout)
        //toNgawi.update(restTemplate.getForObject('http://localhost:8080/core/wim/last/AR', WimEntity.class))
        //toCepu.update(restTemplate.getForObject('http://localhost:8080/core/wim/last/BR', WimEntity.class))
        Thread.startDaemon { monitorWimData(); }
    }

    private generateWeeklyData(startDate, endDate, groupPerGolongan) {
        def chartData = [cat: []];
        { start, end, data ->
            while (start <= end) {
                data.cat << start.format('dd')
                start++
            }
        }.call(startDate, endDate, chartData);

        if (groupPerGolongan) {
            def result = new JsonSlurper().parseText(restTemplate.getForObject('http://localhost:8080/core/wim/statistic/weekly/golongan/{from}/{to}', String.class, startDate.format('yyyy-MM-dd'), endDate.format('yyyy-MM-dd')))
            chartData.serie = []
            ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'].eachWithIndex { itm, idx ->
                chartData.serie << [stack: idx, data: [label: "Gol $itm", value: result[itm].normal]]
                chartData.serie << [stack: idx, data: [label: "Gol $itm Overload", value: result[itm].overload]]
            }

        } else {
            def result = new JsonSlurper().parseText(restTemplate.getForObject('http://localhost:8080/core/wim/statistic/weekly/{from}/{to}', String.class, startDate.format('yyyy-MM-dd'), endDate.format('yyyy-MM-dd')))
            chartData.serie = [
                    [stack: '1', data: [label: 'Normal Vehicle', value: result.normal]],
                    [stack: '1', data: [label: 'Overload Vehicle', value: result.overload]],
            ]
        }
        super.addWindow(new StatisticWindow("Summary of vehicle from " + startDate.format('yyyy/MM/dd') + " to " + endDate.format('yyyy/MM/dd'), chartData))
    }

}
