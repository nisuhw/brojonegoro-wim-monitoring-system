package com.sin.ubs.wim.vaadin;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import javax.servlet.annotation.WebServlet;

/**
 * Created by sin on 3/9/14.
 */
@WebServlet(value = {"/dashboard/*", "/VAADIN/*"}, asyncSupported = true, loadOnStartup = 10)
@VaadinServletConfiguration(productionMode = false, ui = MainUI.class, widgetset = "com.sin.ubs.wim.vaadin.AppWidgetSet")
public class MainServlet extends VaadinServlet {
}
