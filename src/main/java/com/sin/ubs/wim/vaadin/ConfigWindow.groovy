package com.sin.ubs.wim.vaadin

import com.sin.ubs.wim.core.config.ApplicationContextProvider
import com.sin.ubs.wim.core.repository.ConfigRepository
import com.sin.ubs.wim.core.service.ConfigService
import com.vaadin.ui.*

/**
 * Created by sin on 3/14/14.
 */
class ConfigWindow extends Window {

    ConfigWindow() {
        setWidth('300px')
        setHeight('300px')
        center()
        this.caption = 'Update Limit Overload'
        content = new VerticalLayout()
        content.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER)
        content.setSizeFull()
        content.addComponent(new FormLayout().with {
            setSizeUndefined()
            spacing = true

            def config = ApplicationContextProvider.getBean(ConfigService.class).config
            TextField fw, bw, tan, tri;
            addComponent(fw = new TextField('Limit Front Wheel', config.limitFW.toString()))
            addComponent(bw = new TextField('Limit Back Wheel', config.limitBW.toString()))
            addComponent(tan = new TextField('Limit Tandem Wheel', config.limitTandem.toString()))
            addComponent(tri = new TextField('Limit Tridem Wheel', config.limitTridem.toString()))
            addComponent(new Button('Update', {
                config.limitFW = Double.valueOf(fw.value)
                config.limitBW = Double.valueOf(bw.value)
                config.limitTandem = Double.valueOf(tan.value)
                config.limitTridem = Double.valueOf(tri.value)
                ApplicationContextProvider.getBean(ConfigRepository.class).save(config)
                Notification.show('Config Updated')
                super.close()
            }))
            it
        })
    }
}
