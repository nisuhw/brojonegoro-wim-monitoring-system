<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <title>WIM Data</title>
        <script src="/core/webjars/jquery/2.1.0/jquery.min.js"></script>
        <!-- This goes in the document HEAD so IE7 and IE8 don't cry -->
        <!--[if lt IE 9]>
        <style type="text/css">
           table.gradienttable th {
           filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d5e3e4', endColorstr='#b3c8cc',GradientType=0 );
           position: relative;
           z-index: -1;
           }
           table.gradienttable td {
           filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebecda', endColorstr='#ceceb7',GradientType=0 );
           position: relative;
           z-index: -1;
           }
        </style>
        <![endif]-->
        <!-- CSS goes in the document HEAD or added to your external stylesheet -->
        <style type="text/css">
            table.gradienttable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.gradienttable th {
                padding: 0px;
                background: #d5e3e4;
                background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Q1ZTNlNCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iI2NjZGVlMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNiM2M4Y2MiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
                background: -moz-linear-gradient(top,  #d5e3e4 0%, #ccdee0 40%, #b3c8cc 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d5e3e4), color-stop(40%,#ccdee0), color-stop(100%,#b3c8cc));
                background: -webkit-linear-gradient(top,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                background: -o-linear-gradient(top,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                background: -ms-linear-gradient(top,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                background: linear-gradient(to bottom,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                border: 1px solid #999999;
            }
            table.gradienttable td {
                padding: 0px;
                background: #ebecda;
                background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ViZWNkYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iI2UwZTBjNiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjZWNlYjciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
                background: -moz-linear-gradient(top,  #ebecda 0%, #e0e0c6 40%, #ceceb7 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebecda), color-stop(40%,#e0e0c6), color-stop(100%,#ceceb7));
                background: -webkit-linear-gradient(top,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                background: -o-linear-gradient(top,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                background: -ms-linear-gradient(top,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                background: linear-gradient(to bottom,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                border: 1px solid #999999;
            }
            table.gradienttable th{
                margin:0px;
                padding:5px;
                border-top: 1px solid #eefafc;
                border-bottom:0px;
                border-left: 1px solid #eefafc;
                border-right:0px;
            }
            table.gradienttable td{
                margin:0px;
                padding:5px;
                border-top: 1px solid #fcfdec;
                border-bottom:0px;
                border-left: 1px solid #fcfdec;
                border-right:0px;
            }
            table.gradienttable .title{
                height:23px;
            }
            table.gradienttable .field-name{
                text-align: left;
            }
            table.gradienttable .field-value{
                text-align: right;
            }
            .overload{
                font-weight: bolder;
                color: red;
            }
            .deleteBtn{
                text-align: center;
            }
            .last{
                height: 28px;
            }
        </style>
        <script type="text/javascript">
            function confirmDelete(id){
                if(confirm('Are You sure delete this data?')){
                    window.location='/core/data/delete/'+id
                }
            }
            function changePeriode(periode){
                window.location='/core/data/list/overload?period='+periode
            }
            function qs(key) {
                key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
                var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
                return match && decodeURIComponent(match[1].replace(/\+/g, " "));
            }
            $(function(){
                var period=qs('period')
                if(period!=null)
                $('#periodList').val(period)
            })
        </script>
    </head>
    <body>
        <button onclick="window.location='/dashboard'">Back To Dashboard</button>
        Periode : 
        <form:select items="${periodList}" path="periodList" id="periodList" onchange="changePeriode(this.value)" />
        <button onclick="window.print()">Print</button>
        <c:forEach items="${wimList}" var="wim" varStatus="i">
            <table border="1" class="gradienttable">
                <tr>
                    <th colspan="3" class="title">Vehicle Axle Info</th>
                    <th colspan="2" class="title">Vehicle General Info</th>
                    <th colspan="3" class="title">Vehicle Snapshot</th>
                </tr>
                <tr>
                    <th>Num Axle</th>
                    <th>Axle Weight</th>
                    <th>Axle Distance</th>
                    <th class="field-name">Record Number</th>
                    <td class="field-value">${wim.id}</td>
                    <td rowspan="8"><a href="/snapshot-overload/${wim.snapshotFileName}" target="_blank"><img src="/snapshot-overload/${wim.snapshotFileName}" width="420"/></a></td>
                </tr>
                <tr>
                    <th>1</th>
                    <td class="${wim.axleOverload1?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight1}"/>
                    </td>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.distAxle1}"/>
                    </td>
                    <th class="field-name">Lane</th>
                    <td class="field-value">${wim.laneId}</td>
                </tr>
                <tr>
                    <th>2</th>
                    <td class="${wim.axleOverload2?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight2}"/>
                    </td>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.distAxle2}"/>
                    </td>
                    <th class="field-name">Date & Time</th>
                    <td>
                        <fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${wim.measurementTime}"/>
                    </td>
                </tr>
                <tr>
                    <th>3</th>
                    <td class="${wim.axleOverload3?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight3}"/>
                    </td>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.distAxle3}"/>
                    </td>
                    <th class="field-name">Speed</th>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.speed}"/>
                    </td>
                </tr>
                <tr>
                    <th>4</th>
                    <td class="${wim.axleOverload4?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight4}"/>
                    </td>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.distAxle4}"/>
                    </td>
                    <th class="field-name"># of axle</th>
                    <td class="field-value">${wim.totalAxle}</td>
                </tr>
                <tr>
                    <th>5</th>
                    <td class="${wim.axleOverload5?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight5}"/>
                    </td>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.distAxle5}"/>
                    </td>
                    <th class="field-name">Golongan</th>
                    <td class="field-value">${wim.golongan}</td>
                </tr>
                <tr>
                    <th>6</th>
                    <td class="${wim.axleOverload6?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight6}"/>
                    </td>
                    <td class="field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.distAxle6}"/>
                    </td>
                    <th class="field-name">Vehicle Weight</th>
                    <td class="${wim.vehicleOverload?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.grossVehicleWeight}"/>
                    </td>
                </tr>
                <tr class="last">
                    <th>7</th>
                    <td class="${wim.axleOverload7?'overload':''} field-value">
                        <fmt:formatNumber pattern="#,##0.00" value="${wim.axleWeight7}"/>
                    </td>
                    <td>-</td>
                    <td colspan="2" class="deleteBtn" onclick="confirmDelete('${wim.id}')"><button>DELETE THIS DATA</button></td>
                </tr>
            </table>
        </c:forEach>
    </body>
</html>