<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sin
  Date: 5/23/2014
  Time: 5:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script>
        function promptSerial() {
            var kode = prompt('Masukkan code registrasi')
            if (kode != null && kode.length > 0) {
                $.get('/core/demo/register?key=' + kode, function (sukses) {
                    if (sukses) {
                        alert('Registrasi Berhasil. Aplikasi akan berjalan normal.')
                        window.location = '/dashboard'
                    } else {
                        alert('Registrasi Gagal. Kode yang anda masukkan salah.')
                    }
                })
            }
        }
    </script>
</head>
<body>
<h2>Aplikasi ini adalah versi DEMO dan hanya dapat digunakan sampai ${expiredDemo}. Silahkan tekan tombol Lanjut untuk
    melanjutkan, atau tekan tombol Register untuk menghilangkan pesan ini.</h2>

<form action="/core/demo/iframe">
    <c:if test="${lanjut}">
        <button type="submit">Lanjut</button>
    </c:if>
    <button type="button" onclick="promptSerial()">Register</button>
</form>
<br/>
</html>
