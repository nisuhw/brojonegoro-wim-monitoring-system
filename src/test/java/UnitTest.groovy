import com.sin.ubs.wim.core.config.AppConfig
import com.sin.ubs.wim.core.controller.WimController
import com.sin.ubs.wim.core.model.entity.ConfigEntity
import com.sin.ubs.wim.core.model.entity.WimEntity
import com.sin.ubs.wim.core.repository.WimRepository
import com.sin.ubs.wim.core.service.WimService
import org.joda.time.DateTime
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

import java.text.SimpleDateFormat

/**
 * Created by sin on 3/2/14.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
class UnitTest {
    @Autowired
    WimService wimService
    @Autowired
    WimRepository wimRepository
    @Autowired
    WimController wimController

    void test() {
        def date = new SimpleDateFormat('yyyy-MM-dd').parse('2019-04-17')
        println wimController.dailyStatistic(date)
        println wimController.dailyStatisticPerGolongan(date)
    }

    @Test
    void generatedata() {
        def random = new Random()
        def dateTime = new DateTime().minusMonths(8);
        100000.times {
            dateTime = dateTime.plusMinutes(1 + random.nextInt(3))
            println dateTime
            def wimEntity = new WimEntity(measurementTime: dateTime.toDate().toTimestamp())
            def totalAxle = 2 + random.nextInt(6)
            (1..totalAxle).each {
                if (it != totalAxle)
                    wimEntity."distAxleL$it" = wimEntity."distAxleR$it" = 1 + random.nextInt(5)
                wimEntity."axleWeight$it" = (5 + random.nextInt(17)) * 1000
            }
            wimEntity.speed = 1 + random.nextInt(100)
            wimEntity.calculateOverweight(new ConfigEntity())
            wimRepository.save(wimEntity)
            println it
            Thread.sleep(5000)
        }
    }

    void randomdatabuatpakis() {
        wimRepository.save(new WimEntity(
                measurementTime: new Date().toTimestamp(),
                laneId: 'AL',
                speed: 45.3702,
                grossVehicleWeight: 28.3,
                axleWeight1: 6.78,
                axleWeight2: 21.52,
                distAxle1: 3.35,
                golongan: 'II',
                axleOverload1: true,
                axleOverload2: true,
                vehicleOverload: true,
                remark: 'W1 W2 GVW > 16'))
        wimRepository.save(new WimEntity(
                measurementTime: new Date().toTimestamp(),
                laneId: 'BL',
                speed: 38.5342,
                grossVehicleWeight: 34.63,
                axleWeight1: 6.78,
                axleWeight2: 12.24,
                axleWeight3: 3.66,
                axleWeight4: 4.75,
                axleWeight5: 7.2,
                distAxle1: 3.41,
                distAxle2: 6.98,
                distAxle3: 1.33,
                distAxle4: 1.32,
                golongan: 'V',
                axleOverload1: true,
                axleOverload3: true,
                axleOverload4: true,
                axleOverload5: true,
                vehicleOverload: true,
                remark: 'W1 W345 GVW > 24'))
        wimRepository.save(new WimEntity(
                measurementTime: new Date().toTimestamp(),
                laneId: 'AL',
                speed: 53.3979,
                grossVehicleWeight: 10.01,
                axleWeight1: 3.35,
                axleWeight2: 6.66,
                distAxle1: 3.41,
                golongan: 'II'))
        wimRepository.save(new WimEntity(
                measurementTime: new Date().toTimestamp(),
                laneId: 'BR',
                speed: 44.8149,
                grossVehicleWeight: 32.06,
                axleWeight1: 6,
                axleWeight2: 12.33,
                axleWeight3: 2.67,
                axleWeight4: 11.05,
                distAxle1: 3.15,
                distAxle2: 3.56,
                distAxle3: 1.14,
                axleOverload1: true,
                golongan: 'IV',
                remark: 'V1'))
    }
}
